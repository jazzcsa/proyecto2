
package proyecto1;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;
import  java.util.Stack;


public class Proyecto1 {
    //pila
    Stack<Integer> S;
    Stack<Integer> ExP;
    int caidfs = 0;
    // variables generales
    Random rm= new Random();
    SecureRandom pr =new SecureRandom();
    int nDN ;
    int nDV ;
    double p;
    float dU ,d;
    String gP = " " ; 
    grafo G = new grafo();
    grafo gbfs = new grafo();
    grafo gdfsr = new grafo();
      grafo gidfs = new grafo();
    int cndfsr = 0;
    int nodoa;
    
    // se decalran varios contructores
    
    //esta parte es para el dfsr****************************
        int nMAg  ;
      
        nodo VN5[] ;
        arista AN5[] ;
        arista ANDFS[] ;
        
    //esta parte es para el dfsr*************************
          //esta parte es para el idfs****************************
        int nMAgidfs  ;
      
        nodo VN6[] ;
        arista AN6[] ;
        arista ANDF6[] ;
        
    //esta parte es para el idfs*************************
        

   public Proyecto1()
   {  
   } 
   public Proyecto1(int a,int  b)      
   {
       this.nDN = a;
       this.nDV = b; 
   }
    public Proyecto1(int a,double b)       
   {
       this.nDN = a;
       this.p = b;  
   }
     public Proyecto1(int a,float b)       
   {
       this.nDN = a;
       this.dU = b;  
   }
     public Proyecto1 (int a)
     {
         this.nDN=a;
         
         
     }
             
             
   // este es el metodo numero uno
   int metodoER ()
   {
        // se declaran los objetos nodo y arsita que sao vectores
        nodo VN[] = new nodo[nDN];
        arista AN[] = new arista[nDV];
        //variables alaaotias
        int r1;
        int r2;
        // esto es para imprimir 
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("archivo.gv");
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN[i] = new nodo (i+1);
       }
       // se inicializa el arrglo de aristas
        for(int i = 0; i < nDV ; i++)
       { 
           AN[i] = new arista();
       }
       // se contruyen las aristas
       
       for (int i=0; i<nDV; i++)
       {
           // se aignan los nodos de la arista
         r1 = rm.nextInt(nDN);
         r2 = rm.nextInt(nDN);
           AN[i].n1 = VN[r1].nN;
           AN[i].n2 = VN[r2].nN;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
           VN[r1].Used = true;
           VN[r2].Used = true;
           // se verifica si la arista no va al mismo nodo
           if (AN[i].n1 == AN[i].n2)
           {
               i--;
           }
           else 
           {
               // se verifica si la arista no se esta repitiendo 
           for(int j = 0; j<i;j++)
           {
               if((AN[i].n1 == AN[j].n1 &  AN[i].n2 == AN[j].n2)|(AN[i].n1 == AN[j].n2 &  AN[i].n2 == AN[j].n1))
               {
                   i--;
               }
           }
           }
           
       }
       
       
       // esta parte solo es para imprimir en pantallla
       System.out.println("\ngraph ErdosReny {");
       //crea grafo con aristas primero
       for(int i = 0; i<nDV; i++ )
       {
          G.grafo = G.grafo + AN[i].n1 + " -- " +AN[i].n2 + ";\n"  ;
           System.out.println(AN[i].n1 + " -- " +AN[i].n2 + ";" );
       } 
       
       //añade nodos solos, si es que los hay
       for(int i = 0; i<nDN; i++ )
           
       {
           if(VN[i].Used)
           {
               
           }
           else
           {
                 G.grafo = G.grafo + VN[i].nN + ";\n" ;
           System.out.println(VN[i].nN + ";" );
           }
        
       }
       
       System.out.println("\n}");
       
       //hasta aqui es para imprimir en pantalla
       
       //lena el archivo para del grafo
      
        try {
        escribir =  new PrintWriter("archivo.gv", "utf-8") ;
        escribir.println("\ngraph ErdosReny {");
        escribir.print(G.grafo);
        /*
         for(int i = 0; i<nDV; i++ )
       {
         
           escribir.println(AN[i].n1 + " -- " +AN[i].n2 + ";" );
       }*/
         
                escribir.println("\n}");
        escribir.close();
        
        
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
    //hasta aqui el archivo del grafo
      return 0 ;  
   }
   // hasta aqui metodo numero uno
  //*********************************************************************************************************************************************** 
   // aqui empieza metodo numero dos
   int metodoGil ()
   {
       //variable para indicar el nodo de comienzo
           int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
           int nd = 0;
           int nMA;
        // se declaran los objetos nodo y arsita que sao vectores
        nMA = (nDN *(nDN-1))/2;
        nodo VN1[] = new nodo[nDN];
        arista AN1[] = new arista[nMA];
       
       
        // esto es para imprimir 
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("archivoGil.gv");
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN1[i] = new nodo (i+1);
       }
       
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMA;i++)
      {
          AN1[i] = new arista();
      }
      
    
       // se contruyen las aristas
       
       for (int i=0; i<nMA; i++)
       {
           
           
           
           // se aignan los nodos de la arista
        
           AN1[i].n1 = VN1[na].nN;

             AN1[i].n2 = VN1[nd].nN;
             
               // se verifica si la arista es con el mimo nodo
           if (AN1[i].n1 == AN1[i].n2)
           {
               i--;
           }
           
           else if(pr.nextDouble()<p)
           {
               AN1[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN1[i].n1 == AN1[j].n1 &  AN1[i].n2 == AN1[j].n2)|(AN1[i].n1 == AN1[j].n2 &  AN1[i].n2 == AN1[j].n1))
               {
                   i--;
               }
           }
           
           if(nd<nDN-1)
           {
               nd=nd+1;
               
           }
           else
           {
               nd = 0;
               na++;
           }
           
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
          VN1[na].Used = true;
          VN1[nd].Used = true;
           
         
           
       }
       
       
       // esta parte solo es para imprimir en pantallla
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMA; i++ )
       {
           if(AN1[i].existe)
           {
          G.grafo = G.grafo + AN1[i].n1 + " -- " +AN1[i].n2 + ";\n"  ;
           System.out.println(AN1[i].n1 + " -- " +AN1[i].n2 + ";" );
           }
       } 
       
       //añade nodos solos, si es que los hay
       for(int i = 0; i<nDV; i++ )
           
       {
           if(VN1[i].Used)
           {
               
           }
           else
           {
                 G.grafo = G.grafo + VN1[i].nN + ";\n" ;
           System.out.println(VN1[i].nN + ";" );
           }
        
       }
       
       System.out.println("\n}");
       
       //hasta aqui es para imprimir en pantalla
       
       //lena el archivo para del grafo
       
        try {
        escribir =  new PrintWriter("archivogil.gv", "utf-8") ;
        escribir.println("\ngraph gil {");
        escribir.println(G.grafo);
        /*
         for(int i = 0; i<nDV; i++ )
       {
         
           escribir.println(AN[i].n1 + " -- " +AN[i].n2 + ";" );
       }*/
         
                escribir.println("\n}");
        escribir.close();
        
        
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
    //hasta aqui el archivo del grafo
      return 0 ;
      
      
   }
   // hasta aqui metodo numero dos
   //*********************************************************************************************
   // aqui empieza metodo numero tres 
   int metodoGografico ()
   {
       //variable para indicar el nodo de comienzo
           int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
           int nd = 0;
           int nMA;
        // se declaran los objetos nodo y arsita que sao vectores
        nMA = (nDN *(nDN-1))/2;
        nodo VN2[] = new nodo[nDN];
        arista AN2[] = new arista[nMA];
      
        // esto es para imprimir 
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("archivoGografico.gv");
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
         
         // aqui se termina de crear el archivo
        // se contruyen todos los nodos y se les asigna una posisicon
       for(int i = 0; i < nDN ; i++)
       { 
           VN2[i] = new nodo (i+1);
           VN2[i].x = rm.nextInt(100);
           VN2[i].y = rm.nextInt(100);
       }
       
       // se incializan todos los posibles aristas
      for(int i =0;i<nMA;i++)
      {
          AN2[i] = new arista();
      }
      
    
       // se contruyen las aristas
       
       for (int i=0; i<nMA; i++)
       {
           
           
           
           // se aignan los nodos de la arista
        
           AN2[i].n1 = VN2[na].nN;

             AN2[i].n2 = VN2[nd].nN;
             
               // se verifica si la arista es con el mimo nodo
               
               d = (float) Math.sqrt((Math.pow((VN2[nd].x-VN2[na].x), 2))+(Math.pow((VN2[nd].y-VN2[na].y), 2)));
           if (AN2[i].n1 == AN2[i].n2)
           {
               i--;
           }
           
           else if(d<dU)
           {
               AN2[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN2[i].n1 == AN2[j].n1 &  AN2[i].n2 == AN2[j].n2)|(AN2[i].n1 == AN2[j].n2 &  AN2[i].n2 == AN2[j].n1))
               {
                   i--;
               }
           }
           
           if(nd<nDN-1)
           {
               nd=nd+1;
               
           }
           else
           {
               nd = 0;
               na++;
           }
           
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
           
    
           
       }
       
       for(int i = 0;i<nDN;i++)
       {
           for(int j =0;j<nMA;j++)
           {
               if((VN2[i].nN == AN2[j].n1 || VN2[i].nN == AN2[j].n2)&& AN2[j].existe == true)
               {
                   VN2[i].Used = true;
                   
               }
           }
           
       }
           // esta parte solo es para imprimir en pantallla
       System.out.println("\ngraph geo {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMA; i++ )
       {
           if(AN2[i].existe)
           {
          G.grafo = G.grafo + AN2[i].n1 + " -- " +AN2[i].n2 + ";\n"  ;
           System.out.println(AN2[i].n1 + " -- " +AN2[i].n2 + ";" );
           }
       } 
       
       //añade nodos solos, si es que los hay
       for(int i = 0; i<nDN; i++ )
           
       {
           if(VN2[i].Used)
           {
               
           }
           else
           {
                 G.grafo = G.grafo + VN2[i].nN + ";\n" ;
           System.out.println(VN2[i].nN + ";" );
           }
        
       }
       
       System.out.println("\n}");
       
       //hasta aqui es para imprimir en pantalla
       
       //lena el archivo para del grafo
       
        try {
        escribir =  new PrintWriter("archivoGografico.gv", "utf-8") ;
        escribir.println("\ngraph gil {");
        escribir.println(G.grafo);
      
         
                escribir.println("\n}");
        escribir.close();
        
        
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
    //hasta aqui el archivo del grafo
      return 0 ;
      
      
   }
   // hasta aqui metodo numero tres
   
   //***************************************************************************
    // aqui empieza metodo numero cuatro 
   int metodobarabasi ()
   {
           int va = 1;
           int nMA;
           //se asigna el numero maximo de grado de cada nodo
           int gradoM = nDV;
           
        // se declaran los objetos nodo y arsita que sao vectores
        nMA = (nDN *(nDN-1))/2;
        nodo VN3[] = new nodo[nDN];
        arista AN3[] = new arista[nMA];
      
        // esto es para imprimir 
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("archivobara.gv");
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
         
         // aqui se termina de crear el archivo
        // se contruyen todos los nodos y se les asigna una posisicon
       for(int i = 0; i < nDN ; i++)
       { 
           VN3[i] = new nodo (i+1);
          
       }
       
       // se incializan todos los posibles aristas
      for(int i =0;i<nMA;i++)
      {
          AN3[i] = new arista();
      }
      
    
       // se contruyen las aristas
       AN3[0].n1 = VN3[0].nN;
       AN3[0].n2 = VN3[1].nN;
       VN3[0].grado = 1;
       VN3[1].grado = 1;
       VN3[0].pro = 1.0-((double)VN3[0].grado/(double)gradoM);
      
       VN3[1].pro = 1.0-(((double)VN3[1].grado/(double)gradoM));
       AN3[0].existe = true;
       System.out.println(VN3[0].pro);
       System.out.println(VN3[1].pro);

             
       
       for (int i=2; i<nDN; i++)
       {
           
           
           for(int j=0;j<i;j++)
           {
           if(pr.nextDouble()<VN3[j].pro)
        
           {
               AN3[va].n1 = VN3[j].nN;
               AN3[va].n2 = VN3[i].nN;
                VN3[i].grado += 1;
                VN3[j].grado += 1;
                VN3[i].pro = 1.0-((double)VN3[i].grado/(double)gradoM);
                VN3[j].pro = 1.0-((double)VN3[j].grado/(double)gradoM);
               va +=1;
               AN3[va].existe = true;
               
           }
           }
       }
             
           
           
          
           
           
           
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
           
    
           
       
       
      
           // esta parte solo es para imprimir en pantallla
       System.out.println("\ngraph bara {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMA; i++ )
       {
           if(AN3[i].existe){
          G.grafo = G.grafo + AN3[i].n1 + " -- " +AN3[i].n2 + ";\n"  ;
           System.out.println(AN3[i].n1 + " -- " +AN3[i].n2 + ";" );
           }
           
       } 
       
       //añade nodos solos, si es que los hay
     
       
       System.out.println("\n}");
       
       //hasta aqui es para imprimir en pantalla
       
       //lena el archivo para del grafo
       
        try {
        escribir =  new PrintWriter("archivobara.gv", "utf-8") ;
        escribir.println("\ngraph bara {");
        escribir.println(G.grafo);
      
         
                escribir.println("\n}");
        escribir.close();
        
        
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
    //hasta aqui el archivo del grafo
      return 0 ;
      
      
   }
   // hasta aqui metodo numero cuatro
   
   //************************aqui empiezza proyecto 2 
   
   // para bfs se tomara como referencia en dearrollo de gil, por tener mayor posibilidad de conexionesw entre nodos
   
   // aqui empieza metodo bfs
   int bfs ()
   {
       //variable para indicar el nodo de comienzo
        int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
        int nd = 0;
        int nMA;
        int NE;
        int NAC;//no elegido para inicio de bfs
        int ABFS = 0;
        // se declaran los objetos nodo y arsita que sao vectores
        nMA = (nDN *(nDN-1))/2;
        nodo VN1[] = new nodo[nDN];
        arista AN1[] = new arista[nMA];
        arista ANBFS[] = new arista[nMA];
// esto es para imprimir *********************************************************
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("bfs.gv"); 
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN1[i] = new nodo (i+1);
       }  
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMA;i++)
      {
          AN1[i] = new arista();
          ANBFS[i] = new arista();
      }
       // se contruyen las aristas
       for (int i=0; i<nMA; i++)
       {
           // se aignan los nodos de la arista      
           AN1[i].n1 = VN1[na].nN;
           AN1[i].n2 = VN1[nd].nN;
           // se verifica si la arista es con el mimo nodo
           if (AN1[i].n1 == AN1[i].n2)
           {
               i--;
           }
           else if(pr.nextDouble()<0.6)
           {
               AN1[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN1[i].n1 == AN1[j].n1 &  AN1[i].n2 == AN1[j].n2)|(AN1[i].n1 == AN1[j].n2 &  AN1[i].n2 == AN1[j].n1))
               {
                   i--;
                   AN1[i].existe = false;
               }
           }         
           if(nd<nDN-1)
           {
           nd=nd+1;              
           }
           else
           {
               nd = 0;
               na++;
           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          VN1[na].Used = true;
          VN1[nd].Used = true;  
       }
       
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMA; i++ )
       {
           if(AN1[i].existe)
           {
          G.grafo = G.grafo + AN1[i].n1 + " -- " +AN1[i].n2 ;
           System.out.println(AN1[i].n1 + " -- " +AN1[i].n2 + ";" );
           }
       } 
       
       //añade nodos solos, si es que los hay
       for(int i = 0; i<nDV; i++ )   
       {
           if(VN1[i].Used)
           {
            
           }
           else
           {
                 G.grafo = G.grafo + VN1[i].nN + ";\n" ;
           System.out.println(VN1[i].nN + ";" );
           }       
       }      
       System.out.println("\n}");      
 //hasta aqui es para imprimir en pantalla********************************
    NE  = rm.nextInt(nDN);
    VN1[NE].nivel=1;
    NAC = 2;
       System.out.println(VN1[NE].nN +" este es el nodo bueno");
       for(int i = 0; i<AN1.length; i++ ) 
    {
        
        if(AN1[i].existe)
        {
            
            if(VN1[NE].nN == AN1[i].n1  )
            {
                ANBFS[ABFS].n1 = VN1[NE].nN;
                ANBFS[ABFS].n2 = AN1[i].n2;
                VN1[AN1[i].n2-1].nivel = 2;
                ANBFS[ABFS].existe = true;
                //System.out.println("ds " + ANBFS[i].n2+" ds "+VN1[AN1[i].n2-1].nN+" ds "+ VN1[AN1[i].n2-1].nivel);
                System.out.println(ANBFS[ABFS].n1 + " "+VN1[NE].nN+ AN1[i].n2) ;
                ABFS+=1;
            }
            else if(  VN1[NE].nN == AN1[i].n2)
            {
                ANBFS[ABFS].n1 = VN1[NE].nN;
                ANBFS[ABFS].n2 = AN1[i].n1;
                VN1[AN1[i].n1-1].nivel = 2;
                ANBFS[ABFS].existe = true;
                System.out.println(ANBFS[ABFS].n1 + " "+VN1[NE].nN+ AN1[i].n1) ;
                //System.out.println("ds " + ANBFS[i].n1+" ds "+VN1[AN1[i].n1-1].nN+" ds "+VN1[AN1[i].n2-1].nivel);
                ABFS+=1;
            } 
           
        }
    }
    
       System.out.println("este es el valor de :"+ABFS);
       
 for (int j = 0;j<nDN;j++)
 {
     for(int k=0;k<nDN;k++)
     {
         if(VN1[k].nivel == NAC )
         {
               for(int m = 0; m<AN1.length; m++)
                {
                  if(AN1[m].existe)
                        {

                            if(VN1[k].nN == AN1[m].n1 & VN1[AN1[m].n2-1].nivel == 0)
                            {
                                ANBFS[ABFS].n1 = VN1[k].nN;
                                ANBFS[ABFS].n2 = AN1[m].n2;
                                VN1[AN1[m].n2-1].nivel = NAC+1;
                                ANBFS[ABFS].existe = true;
                                ABFS+=1;
                                //System.out.println("ds " + ANBFS[i].n2+" ds "+VN1[AN1[i].n2-1].nN+" ds "+ VN1[AN1[i].n2-1].nivel);
                               
                                
                            }
                            else if(  VN1[NE].nN == AN1[m].n2 & VN1[AN1[m].n1-1].nivel == 0)
                            {
                                ANBFS[ABFS].n1 = VN1[k].nN;
                                ANBFS[ABFS].n2 = AN1[m].n1;
                                VN1[AN1[m].n1-1].nivel = NAC+1;
                                ANBFS[ABFS].existe = true;
                                ABFS+=1;
                                //System.out.println(ANBFS[ABFS].n1 + " "+VN1[NE].nN+ AN1[m].n1) ;
                                //System.out.println("ds " + ANBFS[i].n1+" ds "+VN1[AN1[i].n1-1].nN+" ds "+VN1[AN1[i].n2-1].nivel);
                                
                            } 

                        }  
                }
         
           
         } 
     }
         
     
    NAC+=1; 
 }   

     
      System.out.println("\ngraph bfsprueba {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMA; i++ )
       {
           if(ANBFS[i].existe)
           {
          
           System.out.println(ANBFS[i].n1 + " -- " +ANBFS[i].n2 + ";" );
           }
       } 
        for(int i = 0; i<nMA; i++ )
       {
           if(ANBFS[i].existe)
           {
          gbfs.grafo = gbfs.grafo + ANBFS[i].n1 + " -- " +ANBFS[i].n2 + ";\n" ;
           //System.out.println(AN1[i].n1 + " -- " +AN1[i].n2 + ";" );
           }
       } 
        for(int i = 0;i<nDN;i++)
        {
            if(VN1[i].nivel== 2)
            {
            System.out.println(VN1[i].nN+" "+VN1[i].nivel);
            }
        }
 
       
 //lena el archivo para del grafo*************************************
       
        try {
        escribir =  new PrintWriter("bfs.gv", "utf-8") ;
        escribir.println("\ngraph bfs {");
        escribir.println(gbfs.grafo);    
        escribir.println("\n}");
        escribir.close();      
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui bfs
   
   //desde aqui en dfsr recursivo
    // para dfs se tomara como referencia en dearrollo de gil, por tener mayor posibilidad de conexionesw entre nodos
   //funciona para generar las colas o los nodos conn los que se relaciona cada nodo
   public  void generacola(int g)
   {
      
       
           for(int m = 0; m<AN5.length; m++)
                {
                  if(AN5[m].existe)
                        {

                            if(VN5[g].nN == AN5[m].n1 )
                            {
                              VN5[g].cola2.add(AN5[m].n2);
                            }
                            else if(  VN5[g].nN == AN5[m].n2 )
                            {
                               VN5[g].cola2.add(AN5[m].n1);
                               
                               
                                
                            } 

                        }  
                }  
   }
   //hasta aqui funcion generadora de nodos
   
   //funcion que contruye el grafo final de dfsr
   public void gdfsr(int a)
   {
      // primero se buscan los nodos cercanos
       generacola(a);
       System.out.println(VN5[a].cola2);
       
       for(int i=0; i<(VN5[a].cola2.size());i++)
       {
            if(VN5[VN5[a].cola2.get(i)-1].ex == false)
            {
                VN5[VN5[a].cola2.get(i)-1].ex = true;
                ANDFS[cndfsr].n1 = VN5[a].nN;
                ANDFS[cndfsr].n2 = VN5[VN5[a].cola2.get(i)-1].nN;
                System.out.println(ANDFS[cndfsr].n1);
                System.out.println(ANDFS[cndfsr].n2);
                System.out.println(ANDFS[cndfsr].n1 + " -- " +ANDFS[cndfsr].n2 + ";" );
                gdfsr.grafo = gdfsr.grafo + ANDFS[cndfsr].n1 + " -- " +ANDFS[cndfsr].n2 + ";\n" ;
                ANDFS[cndfsr].existe =true;
                
                System.out.println((VN5[VN5[a].cola2.get(i)-1].nN));
                gdfsr(VN5[VN5[a].cola2.get(i)-1].nN-1);
                
                cndfsr+=1;
                
                
                
            }
       }
       
       
   }
   //hasta aqui funcion que contruye grafo final recursivo
   // aqui empieza metodo bfs
   int dfsr ()
   {
       //variable para indicar el nodo de comienzo
        int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
        int nd = 0;
        int nMA;
        int NE;
        int NAC;//no elegido para inicio de bfs
        int ADFS = 0;
        int po=0;
        nMAg = (nDN *(nDN-1))/2;
        VN5 = new nodo[nDN];
        AN5  = new arista[nMAg];
        ANDFS  = new arista[nMAg];
         
        // se declaran los objetos nodo y arsita que sao vectores
      
// esto es para imprimir *********************************************************
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("dfsr.gv"); 
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN5[i] = new nodo (i+1);
           VN5[i].cola2 = new ArrayList <Integer> (nDN);
       }  
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMAg;i++)
      {
          AN5[i] = new arista();
          ANDFS[i] = new arista();
      }
       // se contruyen las aristas
       for (int i=0; i<nMAg; i++)
       {
           // se aignan los nodos de la arista      
           AN5[i].n1 = VN5[na].nN;
           AN5[i].n2 = VN5[nd].nN;
           // se verifica si la arista es con el mimo nodo
           if (AN5[i].n1 == AN5[i].n2)
           {
               i--;
           }
           else if(pr.nextDouble()<0.6)
           {
               AN5[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN5[i].n1 == AN5[j].n1 &  AN5[i].n2 == AN5[j].n2)|(AN5[i].n1 == AN5[j].n2 &  AN5[i].n2 == AN5[j].n1))
               {
                   i--;
                   AN5[i].existe = false;
               }
           }         
           if(nd<nDN-1)
           {
           nd=nd+1;              
           }
           else
           {
               nd = 0;
               na++;
           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          VN5[na].Used = true;
          VN5[nd].Used = true;  
       }
       
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMAg; i++ )
       {
           if(AN5[i].existe)
           {
          G.grafo = G.grafo + AN5[i].n1 + " -- " +AN5[i].n2 ;
           System.out.println(AN5[i].n1 + " -- " +AN5[i].n2 + ";" );
           }
       } 
       
       //añade nodos solos, si es que los hay
       for(int i = 0; i<nDV; i++ )   
       {
           if(VN5[i].Used)
           {
            
           }
           else
           {
                 G.grafo = G.grafo + VN5[i].nN + ";\n" ;
           System.out.println(VN5[i].nN + ";" );
           }       
       }      
       System.out.println("\n}");      
 //hasta aqui es para imprimir en pantalla********************************
    NE  = rm.nextInt(nDN);
    
    NAC = 2;
    VN5[NE].ex = true;
    System.out.println("el nodo bueno es:" + VN5[NE].nN);

    // primero saber que nodos tiene el nodo inicial 
    
    gdfsr(NE);
        
       
    


       
        try {
        escribir =  new PrintWriter("dfsr.gv", "utf-8") ;
        escribir.println("\ngraph bfs {");
        escribir.println(gdfsr.grafo);    
        escribir.println("\n}");
        escribir.close();      
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui dfsr
   
   
   
   //desde aqui idfs*************************************************
   
   
   //funcion para ingresar abyacentes a pila
   public  void abyacentes(int g)
   {
      
       
           for(int m = 0; m<AN6.length; m++)
                {
                  if(AN6[m].existe)
                        {

                            if(VN6[g].nN == AN6[m].n1 & VN6[AN6[m].n2-1].ex == false )
                            {
                              S.add(AN6[m].n2);
                            }
                            else if(  VN6[g].nN == AN6[m].n2 & VN6[AN6[m].n1-1].ex == false)
                            {
                               S.add(AN6[m].n1);
                               
                               
                                
                            } 

                        }  
                }  
   }
   
   int idfs ()
   {
       //variable para indicar el nodo de comienzo
        int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
        int nd = 0;
        int nMA;
        int NE;
        int NAC;//no elegido para inicio de bfs
        int ADFS = 0;
        int po=0;
        nMAgidfs = (nDN *(nDN-1))/2;
        VN6 = new nodo[nDN];
        AN6  = new arista[nMAgidfs];
        ANDF6  = new arista[nMAgidfs];
        //
        S = new Stack<Integer>();
        ExP  = new Stack<Integer>();
        // se declaran los objetos nodo y arsita que sao vectores
      
// esto es para imprimir *********************************************************
         File archivo ;
         PrintWriter escribir ;
         try {
        archivo = new File("idfs.gv"); 
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN6[i] = new nodo (i+1);
           
           
       }  
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMAgidfs;i++)
      {
          
          AN6[i] = new arista();
          ANDF6[i] = new arista();
      }
       // se contruyen las aristas
       for (int i=0; i<nMAgidfs; i++)
       {
           
           
           // se aignan los nodos de la arista      
           AN6[i].n1 = VN6[na].nN;
           AN6[i].n2 = VN6[nd].nN;
           // se verifica si la arista es con el mimo nodo
           if (AN6[i].n1 == AN6[i].n2)
           {
               i--;
           }
           else if(pr.nextDouble()<0.6)
           {
               
               AN6[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN6[i].n1 == AN6[j].n1 &  AN6[i].n2 == AN6[j].n2)|(AN6[i].n1 == AN6[j].n2 &  AN6[i].n2 == AN6[j].n1))
               {
                   i--;
                   AN6[i].existe = false;
               }
           }         
           if(nd<nDN-1)
           {
           nd=nd+1;              
           }
           else
           {
               nd = 0;
               na++;
           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          VN6[na].Used = true;
          VN6[nd].Used = true;  
       }
       
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMAgidfs; i++ )
       {
           if(AN6[i].existe)
           {
          G.grafo = G.grafo + AN6[i].n1 + " -- " +AN6[i].n2 ;
           System.out.println(AN6[i].n1 + " -- " +AN6[i].n2 + ";" );
           }
       } 
       
          
 //hasta aqui es para imprimir en pantalla********************************
    NE  = rm.nextInt(nDN);
    
    NAC = 2;
   
    System.out.println("el nodo bueno es:" + VN6[NE].nN);
//aqui se hace la parte iterativa del grafo
S.push(VN6[NE].nN);

 System.out.println(S);
 while(!S.empty())
 {
     nodoa = S.pop();
     if(VN6[nodoa-1].ex == false)
     {
         VN6[nodoa-1].ex =true;
         abyacentes(nodoa-1);
         if (ExP.empty())
         {
             ExP.push(nodoa);
         }
         else{
             ANDF6[caidfs].n1 = ExP.pop();
             ANDF6[caidfs].n2 = nodoa;
             ANDF6[caidfs].existe = true;
              gidfs.grafo = gidfs.grafo + ANDF6[caidfs].n1 + " -- " +ANDF6[caidfs].n2 + ";\n" ;
              ExP.push(nodoa);
             caidfs+=1;
             
         }
     }
       System.out.println(S);
     
 }
//hasta aqui parte iterativa del grafo
   
 
       
        try {
        escribir =  new PrintWriter("idfs.gv", "utf-8") ;
        escribir.println("\ngraph bfs {");
        escribir.println(gidfs.grafo);    
        escribir.println("\n}");
        escribir.close();      
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui idfs********************************************************
   
}

